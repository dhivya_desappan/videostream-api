package com.videostream.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.videostream.model.VideoStream;
import com.videostream.repository.VideoStreamRepository;

@Service
public class VideoStreamService {
	
	private static final Logger logger = LoggerFactory.getLogger(VideoStreamService.class);
	
	@Autowired
	private VideoStreamRepository videostreamRepo;

	public VideoStream createVideoStream(VideoStream videostreamDetails) {
		return videostreamRepo.save(videostreamDetails);
	}
	
	

}
