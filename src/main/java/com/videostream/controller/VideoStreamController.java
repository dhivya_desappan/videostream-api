package com.videostream.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.videostream.model.Response;
import com.videostream.model.VideoStream;
import com.videostream.service.VideoStreamService;

@RestController
@RequestMapping(value={"/videostream-api/videostream-service"})
public class VideoStreamController {
	
	@Autowired
	private VideoStreamService videostreamService;
	
	private static final Logger logger = LoggerFactory.getLogger(VideoStreamController.class);
	
	@PostMapping("/videostream-create")
	public Response videostreamCreate(@RequestBody VideoStream videostreamDetails)
	{
		Response response = null;
		try {
			videostreamDetails.setCreatedBy("SYSTEM");
			videostreamDetails.setCreatedDate(new Date());
			videostreamDetails = videostreamService.createVideoStream(videostreamDetails);
			if(videostreamDetails.getVideostreamId() != 0) {
				response = new Response("SUCCESS","New video stream details are inserted successfully!",videostreamDetails);
			}else {
				response = new Response("FAILED","Video stream details are not inserted!",null);
			}
		}catch(Exception e) {
			logger.error("Exception occured in videostreamCreate "+e);
			response = new Response("FAILED", e.getMessage(), null);
		}
		return response;
		
	}

}
