package com.videostream.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.videostream.model.VideoStream;

public interface VideoStreamRepository extends JpaRepository<VideoStream, Long> {

}
