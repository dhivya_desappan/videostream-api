package com.videostream.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="videostream_details")
public class VideoStream {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="videostream_id")
	private Long videostreamId;

	private String filename;
	
	private String filepath;
	
	private Date createdDate;
	
	private String createdBy;

	public Long getVideostreamId() {
		return videostreamId;
	}

	public void setVideostreamId(Long videostreamId) {
		this.videostreamId = videostreamId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
}
