package com.videostream.model;

public class Response {

	private String status;
	private String Messages;
	private Object response;
	
	
	public Response(String status,String message,Object response) {
		this.status = status;
		this.response = response;
		this.Messages = message;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}

	public String getMessages() {
		return Messages;
	}

	public void setMessages(String messages) {
		Messages = messages;
	}

	
}
